import React, { useCallback } from 'react'
import { useEffect, useState } from 'react'
import "./App.css";

const Button = React.memo(function Button(props) {
  console.count("render button");
  return <button {...props} style={{ backgroundColor: "lightgray" }} />;
}, (prev, next) => prev.children === next.children);

const ListItem = React.memo(function ListItem({ children }) {
  console.count("render list item");
  return (
    <li>
      {children}
      <label style={{ fontSize: "smaller" }}>
        <input type="checkbox" />
        Add to cart
      </label>
    </li>
  );
}, (prev, next) => prev.children === next.children)

const App =  React.memo(function App() {
  const [searchString, setSearchString] = useState("");
  const [isSortingDesc, setSortingDesc] = useState(false);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    console.count("render fetch");
    fetch("https://reqres.in/api/products")
      .then((response) => response.json())
      .then((json) =>
        setProducts(
          json.data
            .filter((item) => item.name.includes(searchString))
            .sort((a, z) =>
              isSortingDesc
                ? z.name.localeCompare(a.name)
                : a.name.localeCompare(z.name)
            )
        )
      );
  }, [searchString, isSortingDesc]);

  console.count("render app");

  const handleChangeSearch = useCallback((e) => setSearchString(e.target.value), [])
  const handleSortDesc = useCallback(() => setSortingDesc((value) => !value), []);

  return (
    <div className="App">
      <input
        type="search"
        value={searchString}
        onChange={handleChangeSearch}
      />
      <Button onClick={handleSortDesc}>
        Change sort direction
      </Button>
      <ul>
        {products.map(product => {
          return <ListItem key={`ListItem--${product.id}`}>{product.name}</ListItem>;
        })}
      </ul>
    </div>
  );
})

export default App;
